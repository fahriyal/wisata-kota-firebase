package id.co.firzil.wisatakota;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Fahriyal Afif on 6/1/2016.
 */
public class LihatMap extends AppCompatActivity implements OnMapReadyCallback, LocationListener{

    private LocationManager locationManager;
    private LatLng current;
    private AlertDialog alert;
    private DatabaseReference dbRef;
    private StorageReference storageRef;
    private LatLng latLng;
    private EditText nama_wisata, deskripsi_wisata;
    private ImageView gambar_wisata;
    private Dialog dialog;
    private Uri image_uri;

    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.lihat_map);

        dbRef = FirebaseDatabase.getInstance().getReference("lokasi_wisata");
        storageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://tensile-will-132907.appspot.com");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location now = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(now != null){
            current = new LatLng(now.getLatitude(), now.getLongitude());
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected void onResume(){
        super.onResume();
        cekGps();
    }

    private void cekGps(){
        //jika gps sudah aktif
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 6 * 1000, 0, this);
        }
        else{
            if(alert == null){
                alert = new AlertDialog.Builder(this)
                        .setTitle("Pengaturan GPS")
                        .setMessage("GPS Mati, nyalakan GPS ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .create();
            }
            if(! alert.isShowing()) alert.show();
        }
    }

    protected void onDestroy(){
        super.onDestroy();
        locationManager.removeUpdates(this);
    }

    private HashMap<Marker, Place> marker_data = new HashMap<>();
    private GoogleMap gmap;
    private ArrayList<Polyline> rute;

    private void addMarker(String id, double lat, double lon, String gambar, String nama, String desk){
        Marker marker = gmap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        Place p = new Place(id, gambar, nama, desk);
        marker_data.put(marker, p);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gmap = map;
        gmap.getUiSettings().setMyLocationButtonEnabled(true);
        gmap.setMyLocationEnabled(true);

        final LatLng lt = new LatLng(-7.2674604,112.7400695);
        gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(lt, 10));

        gmap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker m) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker marker) {
                Place p = marker_data.get(marker);

                View v = getLayoutInflater().inflate(R.layout.popup_marker, null);
                TextView nama = (TextView) v.findViewById(R.id.nama_tempat),
                        deskripsi = (TextView) v.findViewById(R.id.deskripsi);
                ImageView gambar = (ImageView) v.findViewById(R.id.gambar);

                nama.setText(p.nama);
                deskripsi.setText(p.deskripsi);

                if(! TextUtils.isEmpty(p.gambar)) {
                    gambar.setImageURI(Uri.parse(p.gambar));
                }

                return v;
            }
        });

        gmap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                LatLng dest = marker.getPosition();
                if(current != null){
                    final String MY_SERVER_KEY = "AIzaSyAg8w6vjUxSi_WoxfONDiioi3wnnFgHwmA";  //ganti dengan server key mu

                    String url = "https://maps.googleapis.com/maps/api/directions/json?"
                            +"origin="+current.latitude+","+current.longitude
                            +"&destination="+dest.latitude+","+dest.longitude
                            +"&key="+MY_SERVER_KEY;


                    new AsyncTask<String, String, String>(){
                        ProgressDialog pd;

                        protected void onPreExecute(){
                            super.onPreExecute();
                            pd = new ProgressDialog(LihatMap.this);
                            pd.setMessage("Getting route, please wait..");
                            pd.setIndeterminate(true);
                            pd.setCanceledOnTouchOutside(false);
                            pd.setCancelable(true);
                            pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    cancel(true);
                                }
                            });
                            pd.show();
                        }

                        @Override
                        protected String doInBackground(String... params) {
                            String data = "";
                            try{
                                //menjalankan url yg dipanggil
                                URL url = new URL(params[0]);

                                // membuat http connection untuk komunikasi dengan url
                                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                                // koneksi dengan url
                                urlConnection.connect();

                                // Membaca data dari url
                                InputStream iStream = urlConnection.getInputStream();

                                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                                StringBuilder sb  = new StringBuilder();

                                String line;
                                while( ( line = br.readLine())  != null){
                                    sb.append(line);
                                }
                                data = sb.toString();
                                br.close();

                                iStream.close();
                                urlConnection.disconnect();

                            }catch(Exception e){
                                Log.d("Exception", e.toString());
                            }
                            return data;
                        }

                        protected void onPostExecute(String result){
                            super.onPostExecute(result);
                            pd.dismiss();

                            try {
                                JSONObject js = new JSONObject(result);
                                JSONArray routes = js.optJSONArray("routes");
                                if(routes != null && routes.length() > 0) {
                                    if(rute == null) rute = new ArrayList<>();
                                    else if(rute.size() > 0){   //dihapus dulu semua rute yg muncul di gmaps
                                        for(int i=0; i<rute.size(); i++) {
                                            Polyline per_rute = rute.get(i);
                                            per_rute.remove();
                                        }
                                        rute.clear();
                                    }

                                    for (int i = 0; i < routes.length(); i++) {
                                        List<LatLng> list_ltln = new ArrayList<>();
                                        JSONObject ob = routes.getJSONObject(i);
                                        JSONArray legs = ob.optJSONArray("legs");
                                        for (int j = 0; j < legs.length(); j++) {
                                            JSONObject per_leg = legs.getJSONObject(j);
                                            JSONArray steps = per_leg.optJSONArray("steps");
                                            for (int k = 0; k < steps.length(); k++) {
                                                String encoded_point = steps.getJSONObject(k).getJSONObject("polyline").getString("points");
                                                list_ltln.addAll(PolyUtil.decode(encoded_point));
                                            }
                                        }

                                        Random rnd = new Random(); //generate warna random
                                        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                        rute.add(gmap.addPolyline(new PolylineOptions().color(color).addAll(list_ltln)));
                                    }
                                }

                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(LihatMap.this, "Error menampilkan rute", Toast.LENGTH_LONG).show();
                            }
                        }

                    }.execute(url);
                }
                else {
                    cekGps();
                    Toast.makeText(LihatMap.this, "Sedang mencari lokasi gps...", Toast.LENGTH_LONG).show();
                }
            }
        });

        dbRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot data, String s) {
                try {
                    System.out.println("Child key = "+data.getKey());
                    final String id = data.getKey();
                    final String nama = data.child("nama").getValue(String.class);
                    final String deskripsi = data.child("deskripsi").getValue(String.class);
                    final double lat = data.child("latitude").getValue(Double.class);
                    final double lon = data.child("longitude").getValue(Double.class);
                    String file_name = data.child("image").getValue(String.class);

                    final File localFile = new File(getFileDirectory() + file_name);
                    if(! localFile.exists()) {
                        localFile.createNewFile();

                        storageRef.child(file_name).getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                addMarker(id, lat, lon, localFile.getPath(), nama, deskripsi);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                e.printStackTrace();
                                Toast.makeText(LihatMap.this, "Exeption when getting image\n" + e.getMessage().trim(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else addMarker(id, lat, lon, localFile.getPath(), nama, deskripsi);
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(LihatMap.this, "Exception when adding data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot data) {
                try{
                    for (Map.Entry<Marker, Place> entry : marker_data.entrySet()) {
                        Marker marker = entry.getKey();
                        Place place = entry.getValue();
                        if(place.id.equals(data.getKey())){
                            System.out.println("data founded = "+data.getKey());
                            marker.remove();
                            marker_data.remove(marker);
                            break;
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(LihatMap.this, "Exception when deleting data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        gmap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng selectedLtLn) {
                latLng = selectedLtLn;
                if(dialog == null){
                    dialog = new Dialog(LihatMap.this);
                    dialog.setContentView(R.layout.tambah_wisata);

                    nama_wisata = (EditText) dialog.findViewById(R.id.nama_wisata) ;
                    deskripsi_wisata = (EditText) dialog.findViewById(R.id.deskripsi_wisata);
                    gambar_wisata = (ImageView) dialog.findViewById(R.id.gambar_wisata);
                    gambar_wisata.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(v.getContext());
                            dialog.setTitle("Ambil Gambar");
                            dialog.setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startCamera();
                                }
                            });
                            dialog.setNegativeButton("File", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startFileChooser();
                                }
                            });
                            dialog.show();
                        }
                    });

                    dialog.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ProgressDialog pd = new ProgressDialog(v.getContext());
                            pd.setMessage("Uploading..");
                            pd.setCanceledOnTouchOutside(false);
                            pd.setCancelable(true);

                            final UploadTask uploadTask = storageRef.child(image_uri.getLastPathSegment()).putFile(image_uri);
                            pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    uploadTask.cancel();
                                }
                            });
                            pd.show();

                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(LihatMap.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                                    pd.dismiss();
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    StorageMetadata m = taskSnapshot.getMetadata();
                                    String nama_file = m.getName();

                                    Map<String, Object> post1 = new HashMap<>();
                                    post1.put("nama", nama_wisata.getText().toString());
                                    post1.put("deskripsi", deskripsi_wisata.getText().toString());
                                    post1.put("image", nama_file);
                                    post1.put("latitude", latLng.latitude);
                                    post1.put("longitude", latLng.longitude);

                                    dbRef.push().setValue(post1, new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                            pd.dismiss();
                                            if(databaseError != null){
                                                Toast.makeText(LihatMap.this, "Tidak bisa menyimpan data\n"+databaseError.getMessage().trim(),
                                                        Toast.LENGTH_LONG).show();
                                            }
                                            else {
                                                image_uri = null;
                                                nama_wisata.setText("");
                                                deskripsi_wisata.setText("");
                                                gambar_wisata.setImageBitmap(null);
                                                Toast.makeText(LihatMap.this, "Data Terimpan", Toast.LENGTH_LONG).show();

                                                dialog.dismiss();
                                            }
                                        }
                                    });
                                }
                            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    double progress = 100.0 * (double) (taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                                    System.out.println("Upload is " + progress + "% done");
                                }
                            });
                        }
                    });
                }
                dialog.show();
            }
        });
    }

    private void startCamera(){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(i.resolveActivity(getPackageManager()) != null){
            String image_path = getFileDirectory() + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
            File f = new File(image_path);
            image_uri = Uri.fromFile(f);
            i.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);

            startActivityForResult(i, 1);
        }
        else Toast.makeText(this, "Tidak ada aplikasi kamera", Toast.LENGTH_LONG).show();
    }

    public void startFileChooser(){
        Intent i = Intent.createChooser(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
                "Pilih aplikasi");
        startActivityForResult(i, 2);
    }

    @Override
    public void onActivityResult(int request, int result, Intent data) {
        super.onActivityResult(request, result, data);
        if(result == Activity.RESULT_OK){
            if(request == 1 || request == 2){
                try{
                    if(request == 2){
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = { MediaStore.Images.Media.DATA };
                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String image_path = cursor.getString(columnIndex);
                        cursor.close();

                        image_uri = Uri.fromFile(new File(image_path));
                    }

                    gambar_wisata.setImageURI(image_uri);
                }
                catch(OutOfMemoryError o){
                    o.printStackTrace();
                    Toast.makeText(this, "Out Of Memory", Toast.LENGTH_LONG).show();
                }
                catch(NullPointerException o){
                    o.printStackTrace();
                    Toast.makeText(this, "NullPointerException", Toast.LENGTH_LONG).show();
                }
                catch(Exception o){
                    o.printStackTrace();
                    Toast.makeText(this, "Exception", Toast.LENGTH_LONG).show();
                }

                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(getFileDirectory()));
                sendBroadcast(intent);
            }
        }
    }

    private String getFileDirectory(){
        File f = new File(Environment.getExternalStorageDirectory() + "/"+getString(R.string.app_name));
        if(! f.exists()) f.mkdirs();

        return f.getPath() + "/";
    }

    @Override
    public void onLocationChanged(Location l) {
        current = new LatLng(l.getLatitude(), l.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class Place{
        public String id, nama, deskripsi, gambar;
        public Place(String id, String gambar, String nama, String deskripsi){
            this.id = id;
            this.nama = nama;
            this.gambar = gambar;
            this.deskripsi = deskripsi;
        }
    }

}
