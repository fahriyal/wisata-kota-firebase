package id.co.firzil.wisatakota;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class WisataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Wisata> list = new ArrayList<>();
    private LayoutInflater inf;
    private Context c;
    private View.OnClickListener onClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Wisata w = (Wisata) v.getTag();
            Intent i = new Intent(c, LihatMap.class);
            i.putExtra("lat", w.latitude);
            i.putExtra("lon", w.longitude);

            c.startActivity(i);
        }
    };

    //cunstructor class
    public WisataAdapter(Context c){
        this.c = c;
        inf = LayoutInflater.from(c);
    }

    //berfungsi menentukan tampilan list
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WisataHolder(inf.inflate(R.layout.per_wisata, parent, false));
    }

    //berfungsi menampilkan data pada list
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WisataHolder h = (WisataHolder) holder;
        Wisata w = list.get(position);

        h.nama.setText(w.nama);
        h.desk.setText(w.deskripsi);
        try{
            h.gambar.setImageURI(Uri.parse(w.gambar));
        }
        catch (OutOfMemoryError oom){
            oom.printStackTrace();
            h.gambar.setImageBitmap(null);
        }

        h.root.setTag(w);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //menambah item wisata
    public void addWisata(Wisata w){
        list.add(w);
        notifyItemInserted(list.size() - 1);
    }

    //menghapus item wisata
    public void deleteWisata(String id){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).id.equals(id)){
                list.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    //class holder view
    private class WisataHolder extends RecyclerView.ViewHolder{
        private TextView nama, desk;
        private ImageView gambar;
        private View root;
        public WisataHolder(View v) {
            super(v);
            root = v;
            nama = (TextView) v.findViewById(R.id.nama_tempat);
            desk = (TextView) v.findViewById(R.id.deskripsi);
            gambar = (ImageView) v.findViewById(R.id.gambar);

            root.setOnClickListener(onClickListener);
        }
    }

}