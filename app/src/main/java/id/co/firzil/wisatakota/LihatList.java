package id.co.firzil.wisatakota;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;

public class LihatList extends AppCompatActivity {

    private WisataAdapter adapter;
    private DatabaseReference dbRef;
    private StorageReference storageRef;

    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.lihat_list);

        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new WisataAdapter(this);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        dbRef = FirebaseDatabase.getInstance().getReference("lokasi_wisata");
        storageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://tensile-will-132907.appspot.com");

        dbRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot data, String s) {
                try {
                    System.out.println("Child key = "+data.getKey());
                    final String id = data.getKey();
                    final String nama = data.child("nama").getValue(String.class);
                    final String deskripsi = data.child("deskripsi").getValue(String.class);
                    final double lat = data.child("latitude").getValue(Double.class);
                    final double lon = data.child("longitude").getValue(Double.class);
                    String file_name = data.child("image").getValue(String.class);

                    final File localFile = new File(getFileDirectory() + file_name);
                    if(! localFile.exists()) {
                        localFile.createNewFile();

                        storageRef.child(file_name).getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                addWisata(id, lat, lon, localFile.getPath(), nama, deskripsi);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                e.printStackTrace();
                                Toast.makeText(LihatList.this, "Exeption when getting image\n" + e.getMessage().trim(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else addWisata(id, lat, lon, localFile.getPath(), nama, deskripsi);
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(LihatList.this, "Exception when adding data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot data) {
                try{
                    adapter.deleteWisata(data.getKey());
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(LihatList.this, "Exception when deleting data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void addWisata(String id, double lat, double lon, String gambar, String nama, String desk){
        Wisata w = new Wisata(id, gambar, nama, desk, lat, lon);
        adapter.addWisata(w);
    }

    private String getFileDirectory(){
        File f = new File(Environment.getExternalStorageDirectory() + "/"+getString(R.string.app_name));
        if(! f.exists()) f.mkdirs();

        return f.getPath() + "/";
    }

}
