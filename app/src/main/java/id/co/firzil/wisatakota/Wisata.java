package id.co.firzil.wisatakota;

/**
 * Created by Fahriyal Afif on 6/21/2016.
 */
public class Wisata {
    public String id, nama, deskripsi, gambar;
    public double latitude, longitude;
    public Wisata(String id, String gambar, String nama, String deskripsi, double latitude, double longitude){
        this.id = id;
        this.nama = nama;
        this.gambar = gambar;
        this.deskripsi = deskripsi;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
